import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native';
import Colors from '../constants/Colors';
import StyledButton from '../components/StyledButtonComponent';
import API from '../constants/api';

const inputHeight=38
export default class EnrollScreen extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isInitial: false,
            enrollCourse: "",
            enrollError: false
        };
    }
    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
        return {
            title: 'Enroll',
            headerTitleStyle: {color:'black'},
            headerTintColor: Colors.primaryColor
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                <View>
                    <View style={styles.inputBorder}>
                        <TextInput
                            autoFocus={true}
                            style={styles.input}
                            onChangeText={enrollCourse => this.setState({ enrollCourse})}
                            placeholder="Enroll Code"
                            maxLength={100}
                            autoCapitalize = 'none'
                        />
                    </View>
                    {this.state.enrollError &&<Text style={{color:"red"}}>Fail to enroll to the class. Check if you got the right enroll code</Text>}
                    <View>
                        <StyledButton title="Enroll class" onPress={this._handleEnrollCourse}/>
                    </View>
                </View>
                </ScrollView>


            </View>
        );
    }

    _handleEnrollCourse = () => {
        AsyncStorage.getItem('userToken').then((authKey) =>{
            console.log(authKey)
            API.post(`/student-enroll?enrollid=${this.state.enrollCourse}&authkey=${authKey}`)
                .then((response)=>{
                    // console.log(response.data)
                    this.setState({enrollError:false})
                    this.props.navigation.navigate("Courses",{refreshCoursePage: true})
                }) .catch((error)=> this.setState({enrollError:true}))
        }).catch((err) =>{
            console.log(err)
        })
        
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight:10,
        paddingTop:10
    },
    contentContainer: {

    },
    input: {
        fontSize: 20,
        height: inputHeight,
    },
    inputBorder: {
        marginBottom:10,
        height: inputHeight,
        paddingHorizontal: 30,
        borderColor: "rgba(0,0,0,0.18)",
        borderWidth: 0.5,
        borderRadius: 5,
        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.1,
        elevation: 1
    },
});

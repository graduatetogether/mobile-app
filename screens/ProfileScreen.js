import React from 'react';
import { AsyncStorage,Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyledButton from '../components/StyledButtonComponent';

import profilePic from '../assets/images/FonProfilePic.png';
import Colors from '../constants/Colors';
export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Profile',
  };

  constructor (props) {
    super(props);
    this.state = {
        firstname: "",
        lastname: "",
        studentid: "",

    };
  }
  componentDidMount(){
    this._retrieveData()
  }

  _retrieveData = async () =>{
    try{
      let value = await AsyncStorage.getItem("studentInfo")
      if (value !==null){
        value = JSON.parse(value)
        this.setState({firstname: value.studentFname, lastname:value.studentLname, studentid: value.studentID})
      }
    } catch (error) {
      console.log(error)
    }
  }
  renderRow(key, value) {
    return (
        <View key={key} style={{ alignSelf: 'stretch', flexDirection: 'row' , paddingBottom:30}}>
            <View style={{ flex: 1, paddingLeft: 20,alignSelf: 'stretch' }}><Text >{key}</Text></View>
            <View style={{ width: '60%',  marginRight: 20,alignSelf: 'stretch',borderBottomWidth:1, borderBottomColor:'#D6D6D6' }}><Text >{value}</Text></View>
        </View>
    );
  }
  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };

  render() {
    // console.log("render profile page")
    // console.log(this.state)
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
      <View style={{flex:1, backgroundColor:'#fff'}}>
        <View style={styles.avatarContainer}>
          <Image source={profilePic} style={styles.avatar}/>
          <TouchableOpacity style={{marginTop:10}} onPress={() => console.log("want to change profile photo")}>
            <Text style={{textAlign:"center", color: Colors.primaryColor}}>Change profile photo</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          {this.renderRow('Firstname', this.state.firstname)}
          {this.renderRow('Lastname', this.state.lastname)}
          {this.renderRow('Student ID', this.state.studentid)}
          {/* {this.renderRow('Email', 'fxxx@mail.com')}
          {this.renderRow('Phone', '08XXXXXXX')} */}

          <View style={{marginBottom: 10}}>
            <StyledButton style={{backgroundColor:'white', borderWidth: 1, borderColor: "#D1D1D1"}} textStyle={{color:"#838383", fontWeight:'100'}} title="Edit Profile" onPress={console.log("Edit profile pressed")}/>
          </View>
          <View>
            <StyledButton style={{backgroundColor:'red'}} title="Logout"  onPress={this._signOutAsync}/>
          </View>
         
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  avatar:{
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    width:120,
    height:120,
    backgroundColor:'#fff',
    borderRadius: 60
  },
  avatarContainer:{
    alignItems:'center',
    justifyContent:'center',
    padding: 20,
    height: '35%',
    backgroundColor:'#f8f8f8'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight:10,
    paddingTop:10
  },
  contentContainer: {

  },
});


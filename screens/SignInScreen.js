import React from 'react';
import {
  AsyncStorage,
  Button,
  Dimensions,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import MonoTextInput from '../components/StyledTextInputComponent';
import StyledButton from '../components/StyledButtonComponent';
import Colors from '../constants/Colors';
import API from  '../constants/api';

export default class SignInScreen extends React.Component {
    static navigationOptions = {
        header: null, 
    //   title: 'Please sign in',
    };
    constructor (props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            failToSignIn: false
        };
    }
    render() {
    const { width, height } = Dimensions.get("window");
    return (
        <SafeAreaView style={{flex:1, backgroundColor:'#fff'}}>
            <View style={styles.container}>
                <ScrollView
                    scrollEnabled={false}
                >
                    <View style={styles.containerLogo}>
                        <View style={styles.logo}>
                            <Text style={{textAlign:"center"}}>LOGO</Text>
                        </View>
                    </View>
                    <MonoTextInput 
                        placeholder="Student ID" 
                        onChangeText={text => this.setState({ email:text })} 
                        maxLength={100}
                    />
                    <MonoTextInput
                        password={true} 
                        placeholder="Password" 
                        secureTextEntry={true}
                        onChangeText={text => this.setState({ password : text})} 
                        maxLength={100}
                    />
                    {this.state.failToSignIn && <Text style={{color:"red"}}>Fail to signin please try again.</Text>}
                    <StyledButton style={{marginTop:10}} title="Log In" onPress={this._signInAsync}/>
                    <TouchableOpacity onPress={this.handleForgetPassword} style={{marginTop:20}}>
                        <Text style={{textAlign:'right', color:Colors.primaryColor }}>Forget password?</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
            <View style={{flexDirection:'row', justifyContent:'center', height: "15%", borderTopWidth:1, borderTopColor:'rgba(0,0,0,0.16)', paddingTop:20}}>
                <Text style = {{color: "#888888"}}>Don't have an account? </Text>
                <TouchableOpacity onPress={this.handleSignUp}>
                    <Text style={{ color:Colors.primaryColor }}>Sign Up.</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
      );
    }
    handleForgetPassword = () => {
        console.log("forgot password is pressed")
    }
    handleSignUp = () => {
        this.props.navigation.navigate('SignUp')
    }
    _signInAsync = async () => {

        await API.post(`/student-login?studentid=${this.state.email}&password=${this.state.password}`)
                .then((response) => {
                    try {
                    console.log(response.data);
                    authKey = response.data.authKey
                    // AsyncStorage.setItem('userToken', response.data.authKey);
                    console.log(authKey)
                    API.get('/student-info?authkey='+authKey)
                        .then((response) =>{
                            data=response.data
                            console.log(JSON.stringify(response.data))
                            
                            AsyncStorage.multiSet([['userToken', response.data.authKey],['studentInfo', JSON.stringify(response.data)]])
                            this.props.navigation.navigate('Courses', {studentSections: data.sections});
                        })
                        .catch((error)=>{
                            console.log('fail to get student info')
                            this.props.navigation.navigate('Courses', {studentSections: []});
                        })
                    }catch(error){
                        console.log(error)
                        AsyncStorage.clear();
                    }
                })
                .catch((error) => {
                    console.log(error)
                    console.log('fail' + this.state.email +' ' + this.state.password)
                    this.setState({failToSignIn:true})
                    // alert(`Cannot Sign up due to ${error}` );
                })
        
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#fff',
    },
    containerLogo:{
        marginTop:40,
        flex:1,
        alignItems: "center",
        justifyContent:"center"
    },
    logo:{
        height: 200,
        width:200,
        marginTop:15,
        marginBottom:15,
        borderWidth: 1,
        borderColor: "#888888"
        
    }
});
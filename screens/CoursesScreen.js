import React from 'react';
import {
  AsyncStorage,
  Button,
  Image,
  Platform,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

import CourseCard from '../components/CourseCard';
import AddButtonComponent from '../components/AddButtonComponent';

import API from '../constants/api';
import Colors from '../constants/Colors';

export default class CoursesScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
        return {
            title: 'Courses',
            headerRight: <AddButtonComponent/>
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            studentSections: this.props.navigation.getParam("studentSections", []) 
        };
    }
    componentDidMount(){
        if(this.state.studentSections.length<=0){
            this._onRefresh()
        }
    }
    // after render this course page check if it must refresh the course page after nav from another apge
    // ex after enroll must update
    componentDidUpdate(){
        console.log("component didupdate")
        if(this.props.navigation.getParam("refreshCoursePage", false)){
            this.props.navigation.setParams({"refreshCoursePage": false})
            this._onRefresh()
            // console.log("refresh")
        }
    }
    _onRefresh = async () => {
        this.setState({refreshing: true});
        let authKey =  await AsyncStorage.getItem("userToken")
        // console.log("authkey",authKey)
        API.get('/student-info?authkey='+authKey).then((response) => {
            // console.log(response.data)
            this.setState({refreshing: false, studentSections: response.data.sections});
        }).catch((error) =>{
            console.log(error)
            this.setState({refreshing: false});
        });

    }
    
    render() {
        return (
        <View style={styles.container}>
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                }
            >
            <View>
                {this.renderCourseCards()}
                {/* <CourseCard nextPage={this.onPressCourseCard} courseID="ICCS 1409" courseTitle="Introduction to NoSleep" courseTime="Tue 10:00-9:50, Thu 10:00-9:50" color="#00B963" shadow="3"/>
            
                <CourseCard courseID="ICCS 1409" courseTitle="Introduction to NoSleep" courseTime="Tue 10:00-9:50, Thu 10:00-9:50" color="#585DD3" shadow="3" />
                <CourseCard courseID="ICCS 1409" courseTitle="Introduction to NoSleep" courseTime="Tue 10:00-9:50, Thu 10:00-9:50" color="#D5539E" shadow="3"/> */}
            </View>
            </ScrollView>

        </View>
        );
    }



    renderCourseCards = () => {
        // console.log("helo")

        if (this.state.studentSections && this.state.studentSections.length<=0){
            return (<Text>You have no course. Press add button Top-Right to enroll</Text>)
        }
        else {
            return this.state.studentSections.map( (sectionInfo,i) =>this.createCourseCard(sectionInfo,i))
        }
    }
    createCourseCard = (sectionInfo,i) => {
        console.log("create course card",sectionInfo)
        //get course detail pass it to param
        return (
            <CourseCard 
                nextPage={this.onPressCourseCard}
                // navigation={this.props.navigation}
                key={i} 
                sectionInfo={sectionInfo} 
                courseID={sectionInfo.courseID}
                courseTitle={sectionInfo.courseName}
                courseTime={sectionInfo.sectionDateAndTime}
                color={sectionInfo.color}
                shadow="3" 
            />
        );
    }
    onPressCourseCard = (sectionInfo) => {
        AsyncStorage.getItem('studentInfo').then((studentInfo)=> {
            // console.log(studentInfo)
            studentInfo = JSON.parse(studentInfo)
            // console.log((`/student-attendance?studentID=${studentInfo.studentID}&authkey=${studentInfo.authKey}&courseID=${sectionInfo.courseID}&sectionNumber=${sectionInfo.sectionNumber}`)    )
            API.get(`/student-attendance?studentID=${studentInfo.studentID}&authkey=${studentInfo.authKey}&courseID=${sectionInfo.courseID}&sectionNumber=${sectionInfo.sectionNumber}`) 
            .then((response)=>{
                this.props.navigation.push('CourseDetail',{attendanceInfo: response.data, title: sectionInfo.courseID, sectionNumber: sectionInfo.sectionNumber})
            }).catch((error)=> console.log("onpresscoursecard calling student-attendance api", error))   
            
        }).catch((error)=>{console.log(error)});
        
    };

  // _handleHelpPress = () => {
  //   WebBrowser.openBrowserAsync(
  //     'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
  //   );
  // };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {

  },
});

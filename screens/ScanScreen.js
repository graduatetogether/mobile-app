import React, { Component } from 'react';
import { Alert, AsyncStorage, Linking, Dimensions, LayoutAnimation, Text, View, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import { BarCodeScanner, Constants, Permissions } from 'expo';
import Colors from '../constants/Colors';
import API from '../constants/api';

export default class ScanScreen extends Component {
    static navigationOptions = {
        title: 'Code Reader',
    };
    constructor (props) {
        super(props);
        this.state = {
            hasCameraPermission: null,
            lastScannedUrl: null,
            classDetail: "ICCS1409 11/11/11 10:00-12:00",
            scanError: false,
            errorMessage: ""
        };
    }

    componentDidMount() {
        this._requestCameraPermission();
    }

    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted',
        });
    };

    _handleBarCodeRead = async (result) => {
        if (result.data !== this.state.lastScannedUrl) {
            // LayoutAnimation.spring();
            this.setState({ lastScannedUrl: result.data });
            
            let dict ={}
            await AsyncStorage.multiGet(["userToken", "studentInfo"],  (err, stores) => {    
                stores.map((result, i, store) => {
                    
                    let key = store[i][0]
                    let val = store[i][1]
                    
                    dict[key]=val
                })
            })
            this.setState({authKey: dict.userToken, studentInfo: JSON.parse(dict.studentInfo)})

            let studentInfo = this.state.studentInfo
            // console.log("THIS IS THE RESULT", result.data)
            // console.log(`check-attendance?authkey=${studentInfo.authKey}&studentID=${studentInfo.studentID}&time=${new Date().toTimeString().split(" ")[0]}&secret=${result.data}`)
            API.post(`check-attendance?authkey=${studentInfo.authKey}&studentID=${studentInfo.studentID}&time=${new Date().toTimeString().split(" ")[0]}&secret=${result.data}&installationid=${Constants.installationId}`)
            .then((response) =>{
                let sectionNumber = response.data.sectionNumber
                let courseID = response.data.courseID
                // console.log(`/student-attendance?studentID=${studentInfo.studentID}&authkey=${studentInfo.authKey}&courseID=${response.data.courseID}&sectionNumber=${response.data.sectionNumber}`) 
                API.get(`/student-attendance?studentID=${studentInfo.studentID}&authkey=${studentInfo.authKey}&courseID=${response.data.courseID}&sectionNumber=${response.data.sectionNumber}`) 
                .then((response)=>{
                    this.props.navigation.push('CourseDetail',{attendanceInfo: response.data, title: courseID, sectionNumber: sectionNumber})
                }).catch((error)=>{ 
                    console.log("onpresscoursecard calling student-attendance api", error); 
                    this.props.navigation.push('CourseDetail',{attendanceInfo: [], title: courseID, sectionNumber: sectionNumber})
                }) 

                // this.props.navigation.push('CourseDetail',{attendanceInfo: response.data, title: response.data.courseID, sectionNumber: response.data.sectionNumber})
            }).catch((error)=>{
                this.setState({errorMessage:error.response.data, scanError:true})
                console.log("check-attendance",error.response.data)
            })
            
        }
    };

    render() {
        return (
        <View style={styles.container}>

            {this.state.hasCameraPermission === null
                ?   <Text>Requesting for camera permission</Text>
                :   this.state.hasCameraPermission === false
                    ?   <Text style={{ color: '#fff' }}>
                            Camera permission is not granted
                        </Text>
                    :   <BarCodeScanner
                            onBarCodeRead={this._handleBarCodeRead}
                            style={{
                                height: Dimensions.get('window').height,
                                width: Dimensions.get('window').width,
                            }}
                        />
            }

            {this._maybeRenderUrl()}

            <StatusBar hidden />
        </View>
        );
    }

    _handlePressUrl = () => {
        Alert.alert(
        'Press Yes to Confirm',
        [
            {
                text: 'Yes',
                onPress:() => this.onConfirm()
                // onPress: () => Linking.openURL(this.state.lastScannedUrl),
            },
            { text: 'No', onPress: () => {} },
        ],
        { cancellable: false }
        );
    };
    
    onConfirm = () => {

        this.props.navigation.navigate("CourseDetail", "title")
    }

    _handlePressCancel = () => {
        this.setState({ lastScannedUrl: null, scanError:false, scanMessage:"" });
    };

    _maybeRenderUrl = () => {
        if (!this.state.scanError) {
            return;
        }

        return (
        <View style={styles.bottomBar}>
            <TouchableOpacity style={styles.url} onPress={this._handlePressCancel}>
                <Text numberOfLines={1} style={styles.urlText}>
                    {this.state.errorMessage}
                    {/* {this.state.lastScannedUrl} */}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.cancelButton}
                onPress={this._handlePressCancel}>
                <Text style={styles.cancelButtonText}>
                    Cancel
                </Text>
            </TouchableOpacity>
        </View>
        );
    };
    }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
    },
    bottomBar: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        // backgroundColor:  'rgba(0,0,0,0.5)',
        backgroundColor:  'rgba(255,0,0,0.5)',
        padding: 15,
        flexDirection: 'row',
    },
    url: {
        flex: 1,
    },
    urlText: {
        color: "#fff",
        fontSize: 20,
    },
    cancelButton: {
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cancelButtonText: {
        color: 'rgba(255,255,255,0.5)',
        fontSize: 18,
    },
});

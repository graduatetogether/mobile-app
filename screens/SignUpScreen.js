import React from 'react';
import {
  AsyncStorage,
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import MonoTextInput from '../components/StyledTextInputComponent'
import StyledButton from '../components/StyledButtonComponent'
import Colors from '../constants/Colors';
import API from '../constants/api';
export default class SignUpScreen extends React.Component {
    static navigationOptions = {
        header: null, 
    //   title: 'Please sign in',
    };
    constructor (props) {
        super(props);
        this.state = {
            studentid: "",
            studentidError: false,
            firstname: "",
            firstnameError: false,
            lastname: "",
            lastnameError: false,
            password: "",
            passwordError: false, 
            repeatpassword: "", 
            repeatpasswordError: false,
            repeatpasswordMismatch: false,
            signUpError: false,
        };
    }
  
    render() {
      return (
        <SafeAreaView style={{flex:1, backgroundColor:'#fff'}}>
            <View style={styles.container}>
                <Text style={{color: Colors.primaryColor, textAlign:"center", fontSize:18, marginVertical:40}}>Sign Up</Text>
                <MonoTextInput 
                    placeholder="Student ID" 
                    onChangeText={studentid => this.setState({ studentid })} 
                    maxLength={100}
                    error={this.state.studentidError}
                />
                
                <MonoTextInput 
                    placeholder="Firstname" 
                    onChangeText={firstname => this.setState({ firstname })} 
                    maxLength={100}
                />
                <MonoTextInput 
                    placeholder="Lastname" 
                    onChangeText={lastname => this.setState({ lastname })} 
                    maxLength={100}
                />
                <MonoTextInput 
                    placeholder="Password" 
                    onChangeText={password => this.setState({ password })} 
                    maxLength={100}
                    secureTextEntry={true}
                />
                <MonoTextInput 
                    placeholder="Repeat Password" 
                    onChangeText={repeatpassword => this.setState({ repeatpassword })} 
                    maxLength={100}
                    secureTextEntry={true}
                />
                {this.state.studentidError &&<Text style={{color:'red', textAlign:"center", }}>StudentID field is required</Text>}
                {this.state.firstnameError &&<Text style={{color:'red', textAlign:"center", }}>Firstname field is required</Text>}
                {this.state.lastnameError &&<Text style={{color:'red', textAlign:"center", }}>Lastname field is required</Text>}
                {this.state.passwordError &&<Text style={{color:'red', textAlign:"center", }}>Password field is required or longer or equal to 6</Text>}
                {this.state.repeatpasswordError &&<Text style={{color:'red', textAlign:"center", }}>Repeatpassword field is required</Text>}
                {this.state.repeatpasswordMismatch &&<Text style={{color:'red', textAlign:"center", }}>password and repeat password field mismatch</Text>}
                {this.state.signUpError &&<Text style={{color:'red', textAlign:"center", }}>Sorry there is an error signing up. Please try again</Text>}
                <StyledButton  style={{marginTop:10}} title="Sign Up" onPress={this.handleSignUp}/>
            </View>
            <View style={{flexDirection:'row', justifyContent:'center', height: "15%", borderTopWidth:1, borderTopColor:'rgba(0,0,0,0.16)', paddingTop:20}}>
                <Text style = {{color: "#888888"}}>Already have an account? </Text>
                <TouchableOpacity onPress={this.handleSignIn}>
                    <Text style={{ color:Colors.primaryColor }}>Sign In.</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
      );
    }
    handleSignIn = () => {
        this.props.navigation.navigate('SignIn')
    }
    handleSignUp = () => {
        //connect to backend signing up
        console.log("signup is pressed")
        let completeField = true;
        if(this.state.studentid.length<=0){
            completeField = false
            this.setState({studentidError:true})
        } else {this.setState({studentidError:false})}
        if(this.state.firstname.length<=0){
            completeField = false
            this.setState({firstnameError:true})
        } else {this.setState({firstnameError:false})}
        if(this.state.lastname.length<=0){
            completeField = false
            this.setState({lastnameError:true})
        } else {this.setState({lastnameError:false})}
        if(this.state.password.length<6){
            completeField = false
            this.setState({passwordError:true})
        } else {this.setState({passwordError:false})}
        if(this.state.repeatpassword <6){
            completeField=false
            this.setState({repeatpasswordError:true})
        } else {this.setState({repeatpasswordError:false})}
        if(this.state.repeatpassword !== this.state.password){
            completeField=false
            this.setState({repeatpasswordMismatch:true})
        } else {this.setState({repeatpasswordMismatch:false})}
        if(completeField){
            API.post(`/student-register?studentid=${this.state.studentid}&studentfname=${this.state.firstname}&studentlname=${this.state.lastname}&password=${this.state.password}`)
            .then((response) => {
                console.log(response.data);
                this.props.navigation.navigate('SignIn')
            })
            .catch((error) => {
                console.log(error)
                this.setState({signUpError:true})
                // alert(`Cannot Sign up due to ${error}` );
            })
        }
        
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#fff',
    },
    containerLogo:{
        marginTop:50,
        height: '40%',
        alignItems: "center",
        justifyContent:"center"
    },
    logo:{
        height: 200,
        width:200,
        marginTop:15,
        borderWidth: 1,
        
    }
});
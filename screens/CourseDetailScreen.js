import React from 'react';
import {
    AsyncStorage,
    RefreshControl,
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import Colors from '../constants/Colors';
import StyledButton from '../components/StyledButtonComponent';
import API from '../constants/api';
const inputHeight=38
export default class CourseDetailScreen extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            refreshing: false,
            studentInfo: {},
            authKey: "",
            studentSectionAttendance: []
        };
    }
    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
        return {
            title: params.title,
            headerTitleStyle: {color:'black'},
            headerTintColor: Colors.primaryColor
        }
    };
    componentDidMount(){
        // could refresh with detail is call
        // this._onRefresh()
        let attendanceInfo = this.props.navigation.getParam("attendanceInfo", null)
        if (attendanceInfo) {
            this.setState({studentSectionAttendance: attendanceInfo})
        }
    }

    _onRefresh = async () => {
        this.setState({refreshing: true});
        // console.log("in _onRefresh")
        let dict ={}
        await AsyncStorage.multiGet(["userToken", "studentInfo"],  (err, stores) => {    
            stores.map((result, i, store) => {
                
                let key = store[i][0]
                let val = store[i][1]
                
                dict[key]=val
            })
        })
        this.setState({authKey: dict.userToken, studentInfo: JSON.parse(dict.studentInfo)})
        // console.log("fromstor:", dict)
        // console.log("this.state", this.state)
        let studentInfo = this.state.studentInfo
        // this.setState({refreshing: false})
        // console.log("param:", this.props.navigation.getParam("attendanceInfo", "attendanceInfo not found"))
        // console.log("navsatpar:",this.props.navigation.getParam("title", "title not found"))
        // console.log(`/student-attendance?authkey=${this.state.authKey}&studentID=${studentInfo.studentID}&courseID=${this.props.navigation.getParam("title", "title not found")}&sectionNumber=${this.props.navigation.getParam("sectionNumber")}`)
        API.get(`/student-attendance?authkey=${this.state.authKey}&studentID=${studentInfo.studentID}&courseID=${this.props.navigation.getParam("title", "title not found")}&sectionNumber=${this.props.navigation.getParam("sectionNumber")}`)
            .then((response) => {
                console.log("response.data",response.data)
                this.setState({refreshing: false, studentSectionAttendance: response.data});
            }).catch((error) =>{
                console.log(error)
                this.setState({refreshing: false});
            });

    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                        />
                    }
                >
                {
                    this.state.studentSectionAttendance.length>0?
                            
                        [
                            this.renderRow(true, 0, 'Date', 'Time', 'Room', 'Status'),
                            <View key={1} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                {
                                    this.state.studentSectionAttendance.map((data,i) => { // This will render a row for each data element.
                                        return this.renderRow(false, i, data.date.replace(/-/g, '/'), data.time, data.room, data.status);
                                    })
                                }
                            </View>
                        ]
                        : (<Text>You have no attendance history. Please be on time on the upcoming class ^^</Text>)
                }
                </ScrollView>


            </View>
        );
  }
  renderRow(header, key, date, time, room, status) {
    headerStyle = {fontWeight: 'bold'}
    renderStyle = {}
    statusStyle = {}
    if (header){
        renderStyle = headerStyle
    } else {
        if (status==='Present'){
            statusStyle ={color:Colors.primaryColor}
        } else {
            statusStyle = {color:'red'}
        }
    }
    
    return (
        <View key={key} style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' , paddingBottom:20}}>
            <View style={{ flex: 1, alignSelf: 'stretch' }}><Text style={renderStyle}>{date}</Text></View>
            <View style={{ flex: 1, alignSelf: 'stretch' }}><Text style={renderStyle}>{time}</Text></View>
            <View style={{ flex: 1, alignSelf: 'stretch' }}><Text style={renderStyle}>{room}</Text></View>
            <View style={{ flex: 1, alignSelf: 'stretch' }}><Text style={[renderStyle, statusStyle]}>{status}</Text></View>
        </View>
    );
}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight:10,
        paddingTop:10
    },
    contentContainer: {

    },
    input: {
        fontSize: 20,
        height: inputHeight,
    },
    inputBorder: {
        marginBottom:10,
        height: inputHeight,
        paddingHorizontal: 30,
        borderColor: "rgba(0,0,0,0.18)",
        borderWidth: 0.5,
        borderRadius: 5,
        shadowColor: "#000000",
        shadowOffset: {
        width: 0,
        height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 0.1,
        elevation: 1
    },
});

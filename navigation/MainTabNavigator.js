import React from 'react';
import { Image, Platform, View, TouchableOpacity} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';


import CoursesScreen from '../screens/CoursesScreen';
import ScanScreen from '../screens/ScanScreen';
import ProfileScreen from '../screens/ProfileScreen';
import EnrollScreen from '../screens/EnrollScreen';
import CourseDetailScreen from '../screens/CourseDetailScreen';

import bookIcon from '../assets/icons/open-magazine.png';
import profileIcon from '../assets/icons/avatar.png';
import qrCodeIcon from '../assets/icons/qr-code.png'

const CoursesStack = createStackNavigator({
  Courses: CoursesScreen,
  Enroll: EnrollScreen,
  CourseDetail: CourseDetailScreen
});

CoursesStack.navigationOptions = {
  tabBarLabel: 'Courses',
  tabBarIcon: ({ focused, tintColor }) => (
    <Image source={bookIcon} resizeMode={'contain'} style={{flex:1, tintColor: tintColor, margin: 5.5, width:'100%', height: '100%'}}/>
    // <TabBarIcon
    //   focused={focused}
    //   name={
    //     Platform.OS === 'ios'
    //       ? `ios-information-circle${focused ? '' : '-outline'}`
    //       : 'md-information-circle'
    //   }
    // />
  ),    
};

const ScanStack = createStackNavigator({
  Scan: ScanScreen,
});

ScanStack.navigationOptions = {
  tabBarLabel: 'Scan',
  tabBarIcon: ({ focused, tintColor }) => (
    <Image source={qrCodeIcon} resizeMode={'contain'} style={{flex:1, tintColor: tintColor, margin: 5.5, width:'100%', height: '100%'}}/>
  ),
};

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused, tintColor }) => (
    <Image source={profileIcon} resizeMode={'contain'} style={{flex:1, tintColor: tintColor, margin: 5.5, width:'100%', height: '100%'}}/>
  ),
};

export default createBottomTabNavigator({
  CoursesStack,
  ScanStack,
  ProfileStack,
}, {
  tabBarOptions: {
      // showLabel: false, // hide labels
      activeTintColor: '#00B963', // active icon color
      // inactiveTintColor: '#586589',  // inactive icon color
      // style: {
      //     backgroundColor: '#171F33' // TabBar background
      // }
  }
});

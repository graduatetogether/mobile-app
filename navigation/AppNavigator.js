import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';

import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
// creating enroll page
// const EnrollStack = createStackNavigator({
//   Enroll: EnrollScreen,
// });

// // creating detail page
// const CourseDetailStack = createStackNavigator({
//   CourseDetail: CourseDetailScreen,
// });

//so there is no two top header
MainTabNavigator.navigationOptions = {
  header: null
};
const AppStackWithoutDirectMessage = createStackNavigator({
  Main: MainTabNavigator
});

const AuthStack = createStackNavigator({SignIn: SignInScreen, SignUp: SignUpScreen});

export default createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Main: AppStackWithoutDirectMessage,
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStack
}, {
  initialRouteName: 'AuthLoading',
}
);
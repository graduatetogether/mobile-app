import React from "react";
import {
  Image,
  TouchableOpacity,
  View
} from "react-native";
import TabBarIcon from '../components/TabBarIcon';
import { withNavigation } from "react-navigation";

class AddButton extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.push('Enroll')}>
        <View style={{paddingRight: 15}}>
          <TabBarIcon name="ios-add-circle-outline" focused={true} size={30}/>
        </View>
      </TouchableOpacity>
    );
  }
}

export default withNavigation(AddButton);

import React from "react";
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity } from "react-native";
import Colors from '../constants/Colors';
import PropTypes from "prop-types";

export default class StyledButton extends React.Component {
  static propTypes = {
    theme: PropTypes.string,
    title: PropTypes.string,
    size: PropTypes.string,
    loading: PropTypes.bool
  };

  render () {
    const { theme, title, size, loading } = this.props;

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        {...this.props}
        style={[
          styles.defaultButton,
          theme === "outline" && styles.outlineButton,
          size === "lg" && styles.lgButton,
          this.props.style,
          this.props.disabled && { opacity: 0.5 }
        ]}
      >
        <Text
          style={[
            styles.defaultButtonText,
            theme === "outline" && styles.outlineButtonText,
            this.props.textStyle,
            size === "lg" && styles.lgButtonText
          ]}
        >
          {title}
        </Text>
        {loading && <ActivityIndicator/>}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  defaultButton: {
    height: 38,
    borderRadius: 5,
    backgroundColor: Colors.primaryColor,
    alignItems: "center",
    justifyContent: "center",
    // shadowColor: "#000000",
    // shadowOffset: {
    //   width: 0,
    //   height: 3
    // },
    // shadowRadius: 6,
    // shadowOpacity: 0.16,
    elevation: 1,
    paddingHorizontal: 25,
    flexDirection: "row"
  },
  outlineButton: {
    backgroundColor: "#ffffff",
    borderColor: Colors.primaryColor,
    borderWidth: 1
  },

  defaultButtonText: {
    fontSize: 20,
    color: "#ffffff"
  },

  outlineButtonText: {
    color: Colors.primaryColor
  },
  lgButton: {
    height: 45
  },
  lgButtonText: {
    fontSize: 25
  }
});

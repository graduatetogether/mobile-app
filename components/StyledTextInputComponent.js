import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

const inputHeight=40

export default class MonoTextInput extends React.Component {
  render() {
    return (
        <View style={styles.inputBorder}>
            <TextInput
                {...this.props}
                // autoFocus={true}
                style={[this.props.style,  styles.input ]} 
                // onChangeText={text => this.setState({ text })}
                // placeholder=this
                // maxLength={100}
            />
        </View>
        // <TextInput {...this.props} style={[this.props.style, { fontFamily: 'space-mono' }]} />
    );
  }
}

const styles = StyleSheet.create({
    input: {
        fontSize: 14,
        height: inputHeight,
        color:"#DBDBDB"
    },
    inputBorder: {
        marginTop:5,
        marginBottom:5,
        height: inputHeight,
        paddingHorizontal: 15,
        // marginHorizontal: 20,
        borderColor: "#DBDBDB",
        borderWidth: 1,
        borderRadius: 3,
        // shadowColor: "#000000",
        // shadowOffset: {
        //     width: 0,
        //     height: 3
        // },
        // shadowRadius: 10,
        // shadowOpacity: 0.1,
        // elevation: 1,
        backgroundColor: "#F8F8F8"
    },
});
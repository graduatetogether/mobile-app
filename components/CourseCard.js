import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';

export default class CourseCard extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>this.props.nextPage(this.props.sectionInfo)}> 
                    <View flexDirection="row">
                        <CardView
                        cardElevation={this.props.shadow}
                        cardMaxElevation={this.props.shadow}
                        cornerRadius={5}
                        style={styles.card}
                        >
                            <View style={[styles.titleView, {backgroundColor:this.props.color, borderTopLeftRadius:5, borderTopRightRadius: 5} ]}>
                                <Text style={styles.title}>{this.props.courseID}</Text>
                            </View>
                            <View style={styles.contentView}>
                                <Text style={[styles.text,{color: this.props.color}]}>{this.props.courseTitle}</Text>
                                <Text style={[styles.text,{color: this.props.color}]}>{this.props.courseTime}</Text>
                            </View>
                        </CardView>
                    </View>
                </TouchableOpacity>
            </View>
            
        );
    }

}


const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1
    },
    container: {
        flex: 1
        // backgroundColor: '#EEEEEE',
    },
    card: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        flex: 1,
        margin: 20
    },
    contentView: {
        padding:20,
        width: '100%'
    },
    text: {
      textAlign: 'right',
      padding: 10, 
      fontSize: 14,
      fontWeight: "600"
    },
    titleView: {
        padding: 15,
        width:"100%"
    },
    title: {
        fontSize: 20,
        color: 'white',
        textAlign: 'right',
        fontWeight: "600"
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    }
  });

import axios from "axios";

let isRefreshing = false;
let refreshSubscribers = [];

// const base_URL = "10.5.55.31"
const base_URL = "192.168.1.53"
// const base_URL = "localhost"
const base_port ="8080"
const API_URL = "http://"+base_URL+":"+base_port;
axios.defaults.headers.common["Accept"] = "application/json";

if (__DEV__) {
  axios.defaults.headers.common["Origin"] = "http://"+base_URL+":"+base_port;
}

const API = axios.create({
  baseURL: API_URL
});


export default API;